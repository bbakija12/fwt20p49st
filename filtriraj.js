function postaviRaspored(imeRasporeda) {
    const raspored = document.getElementById(imeRasporeda);
}

function filtrirajPredmet(stringPredmet) {

    if(raspored == null){
        console.log("Treba se postaviti raspored!");
    }
    
    var trElement = raspored.getElementsByTagName("tr");

    trElementSize = Object.size(trElement);

    for(var index= 1; index<trElementSize; index++){
        var tdElement = trElement[index].getElementsByTagName("td");
        var tdElementSize = Object.size(tdElement);

        for(var indexTD= 1; indexTD < tdElementSize; indexTD++){
            tdElement[indexTD].classList.remove("hidden");

            if(!tdElement[indexTD].innerHTML.toLowerCase().match(stringPredmet.toLowerCase()) && tdElement[indexTD].innerHTML != "")
                tdElement[indexTD].classList.add("hidden");
        }
    }
}

function filtrirajTip(stringPredmet) {
    
    var trElement = raspored.getElementsByTagName("tr");

    trElementSize = Object.size(trElement);

    for(var index= 1; index<trElementSize; index++){
        var tdElement = trElement[index].getElementsByTagName("td");
        var tdElementSize = Object.size(tdElement);

        for(var indexTD= 1; indexTD < tdElementSize; indexTD++){
            
            if(!tdElement[indexTD].innerHTML.toLowerCase().match(stringPredmet.toLowerCase()) && tdElement[indexTD].innerHTML != ""){
                tdElement[indexTD].classList.add("hidden");
            }
        }
    }
}

function filtrirajTrajanje(stringPredmet) {
    if(stringPredmet == ''){
        return;
    }

    var trajanje = parseInt(stringPredmet);
    
    var trElement = raspored.getElementsByTagName("tr");

    trElementSize = Object.size(trElement);

    for(var index= 1; index<trElementSize; index++){
        var tdElement = trElement[index].getElementsByTagName("td");
        var tdElementSize = Object.size(tdElement);

        for(var indexTD= 1; indexTD < tdElementSize; indexTD++){
            if(tdElement[indexTD].colSpan > trajanje/30){
                tdElement[indexTD].classList.add("hidden");
            }
        }
    }
}

function filtrirajProslo(stringPredmet) {
    if(stringPredmet == ''){
        return;
    }
   
    var trElement = raspored.getElementsByTagName("tr");

    trElementSize = Object.size(trElement);

    for(var index= 1; index<trElementSize; index++){
        var tdElement = trElement[index].getElementsByTagName("td");
        var tdElementSize = Object.size(tdElement);

        for(var indexTD= 1; indexTD < tdElementSize; indexTD++){
            if(tdElement[indexTD].innerHTML != ""){
                    tdElement[indexTD].classList.add("hidden");

            }

            if(tdElement[0].innerHTML == stringPredmet){
                return;
            }
        }
    }
}

function filtrirajBuduce(stringPredmet) {

    var bool=false;

    if(stringPredmet == ''){
        return;
    }
   
    var trElement = raspored.getElementsByTagName("tr");

    trElementSize = Object.size(trElement);

    for(var index= 1; index<trElementSize; index++){
        var tdElement = trElement[index].getElementsByTagName("td");
        var tdElementSize = Object.size(tdElement);

        if(tdElement[0].innerHTML == stringPredmet){
            bool = true;
            continue;
        }

        if(bool == true){
            for(var indexTD= 1; indexTD < tdElementSize; indexTD++){
                if(tdElement[indexTD].innerHTML != ""){
                        tdElement[indexTD].classList.add("hidden");
    
                }
    
            }
        }

    }
}




