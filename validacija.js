function validacijaImena(inputNazivPredmeta){
    let validated = true;
    if (inputNazivPredmeta.value.length < 2 || inputNazivPredmeta.value.length > 5){
        validated = false;
    }
    for(let i=0; i<inputNazivPredmeta.value.length; i++) {
            if(i<inputNazivPredmeta.value.length-1 && (inputNazivPredmeta.value[i] != inputNazivPredmeta.value[i].toUpperCase() || parseInt(inputNazivPredmeta.value[i])>0)) {
                validated=false;
            }
            if(i==inputNazivPredmeta.value.length-1 && ((parseInt(inputNazivPredmeta.value[i])<0 || parseInt(inputNazivPredmeta.value[i])>9) || (inputNazivPredmeta.value[i] != inputNazivPredmeta.value[i].toUpperCase()))) {
                validated=false;
            }
    }

    return validated;
}

function validacijaPocetkaVremena(inputVrijemePocetka){
    let validated = true;

    if(parseInt(inputVrijemePocetka.value[0]+inputVrijemePocetka.value[1]) >= 20 || parseInt(inputVrijemePocetka.value[0]+inputVrijemePocetka.value[1]) <= 7){
        validated = false;
    }

    return validated;
}

function validacijaKrajaVremena(inputVrijemePocetka, inputVrijemeKraja) {
    let validated = true;
    if(parseInt(inputVrijemeKraja.value[0]+inputVrijemeKraja.value[1]) == parseInt(inputVrijemePocetka.value[0]+inputVrijemePocetka.value[1])) {
        if(parseInt(inputVrijemeKraja.value[3]+inputVrijemeKraja.value[4]) <= parseInt(inputVrijemePocetka.value[3]+inputVrijemePocetka.value[4])) {
            validated = false;
        }
    }
    if(parseInt(inputVrijemeKraja.value[0]+inputVrijemeKraja.value[1]) < parseInt(inputVrijemePocetka.value[0]+inputVrijemePocetka.value[1])) {
        validated = false;
    }

    return validated;
}

function validacijaTipa(inputVrijemePocetka, inputVrijemeKraja, inputTip) {
    let validated = true;
    let vrPocetka, vrKraja;
    vrPocetka=(parseInt(inputVrijemePocetka.value[0]+inputVrijemePocetka.value[1])*60)+parseInt(inputVrijemePocetka.value[3]+inputVrijemePocetka.value[4]);
    vrKraja=parseInt(inputVrijemeKraja.value[0]+inputVrijemeKraja.value[1])*60+ parseInt(inputVrijemeKraja.value[3]+inputVrijemeKraja.value[4]);
    let razlika = vrKraja - vrPocetka;

    if(razlika<0) {
        validated = false;
    }
    if(inputTip.value=="predavanje") {
        if(razlika > 180 || razlika < 60) {
            validated = false;
        }
    }
    if(inputTip.value=="vjezbe") {
        if(razlika<45 || razlika>180) {
            validated = false;
        }
    }

    return validated;
}
