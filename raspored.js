var submitBtn = document.getElementById("submitButton");
var forma = document.getElementById("forma");

function resetInputs(){
    forma[0].value = '';
    forma[1].value = '';
    forma[2].value = '';
    forma[3].value = '';
    forma[4].value = '';
}

forma[0].addEventListener('focus', function(){
    resetInputs();
});
forma[1].addEventListener('focus', function(){
    resetInputs();
});
forma[2].addEventListener('focus', function(){
    resetInputs();
});
forma[3].addEventListener('focus', function(){
    resetInputs();
});

submitBtn.addEventListener('click', function(){
    postaviRaspored('raspored');
    filtrirajPredmet(forma[0].value);
    filtrirajTip(forma[1].value);
    filtrirajTrajanje(forma[2].value);
    if(forma[4].value == '+'){
        filtrirajBuduce(forma[3].value);
    }
    if(forma[4].value == '-'){
        filtrirajProslo(forma[3].value);
    }
});