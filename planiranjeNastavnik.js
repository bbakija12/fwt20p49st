var inputNazivPredmeta = document.getElementById("nazivPredmeta");
var inputVrijemePocetka = document.getElementById("vrijemePocetka");
var inputVrijemeKraja = document.getElementById("vrijemeKraja");
var inputTip = document.getElementById("Tip");

var aktivni = document.activeElement;

inputNazivPredmeta.addEventListener('focus', validacijaUkupna);
inputVrijemePocetka.addEventListener('focus', validacijaUkupna);
inputVrijemeKraja.addEventListener('focus', validacijaUkupna);
inputTip.addEventListener('focus', validacijaUkupna);

function validacijaUkupna(){
    trenutniAktivni = document.activeElement;
        if(trenutniAktivni != inputNazivPredmeta){
            let validated = validacijaImena(inputNazivPredmeta);
        
            if (validated){ 
                inputNazivPredmeta.style.backgroundColor = "lightgreen";
            } else {
                inputNazivPredmeta.style.backgroundColor = "pink";
            }
        }
        
        if(trenutniAktivni != inputVrijemePocetka){
            let validated = validacijaPocetkaVremena(inputVrijemePocetka);
        
            if (validated){ 
                inputVrijemePocetka.style.backgroundColor = "lightgreen";
            } else {
                inputVrijemePocetka.style.backgroundColor = "pink";
            }
        }
        
        if(trenutniAktivni != inputVrijemeKraja){
            let validated = validacijaKrajaVremena(inputVrijemePocetka,inputVrijemeKraja);
        
            if (validated){ 
                inputVrijemeKraja.style.backgroundColor = "lightgreen";
            } else {
                inputVrijemeKraja.style.backgroundColor = "pink";
            }
        }
        
        if(trenutniAktivni != inputVrijemeKraja){
            let validated = validacijaTipa(inputVrijemePocetka,inputVrijemeKraja, inputTip);
        
            if (validated){ 
                inputTip.style.backgroundColor = "lightgreen";
            } else {
                inputTip.style.backgroundColor = "pink";
            }
        }
    
        aktivni = document.activeElement;
}